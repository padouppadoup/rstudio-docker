## By MW, Jan 2018, GPLv3+
FROM debian:9

## Update stuff
RUN apt-get update
RUN apt-get install -y wget
RUN apt-get install -y r-base
RUN apt-get install -y gstreamer1.0-plugins-base iso-codes libcap2 libcap2-bin \
    libcdparanoia0 libgstreamer-plugins-base1.0-0 libgstreamer1.0-0 libogg0 \
    libopus0   liborc-0.4-0 libpam-cap libtheora0 libvisual-0.4-0 libvorbis0a \
    libvorbisenc2
RUN apt-get clean
RUN wget https://download1.rstudio.org/rstudio-xenial-1.1.414-amd64.deb
RUN dpkg -i rstudio-xenial-1.1.414-amd64.deb

RUN apt-get install -y libxslt1.1 libxkbcommon-dev libxcb-xkb-dev ## We need that to get the keyboard working
RUN echo 'options(repos = c(CRAN = "https://cran.rstudio.com"))' >.Rprofile
RUN Rscript -e 'install.packages("shiny")'

CMD /usr/bin/rstudio

## docker run -ti --rm -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix rstudio